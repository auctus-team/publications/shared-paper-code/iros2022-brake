# From a trapezoidal acceleration profile to a learnt time optimal control policy for robot braking
*by Arthur Esquerre-Pourtère, Nicolas TORRES ALBERTO and Vincent PADOIS*
*submitted at [IROS2022](https://iros2022.com/) (pending acceptance)*

## Abstract
  Time-optimal robot trajectories are usually generated taking into account sub-optimal but constant capacities along the workspace. These maximal values correspond to lower boundaries for the acceleration and jerk, reachable in any joint configuration. Nevertheless, the non-linear relation between the joint torques, their acceleration and jerk is highly dependent on the joint configuration of the robot.

In this paper, we propose a method to control a robot without making the hypothesis of constant maximum acceleration and jerk in the case of braking in order to make a better use of the robot's real capacities. 
To achieve this, a policy giving the optimal joint torque at each time step is learned via deep reinforcement learning. A method based on the classical literature is used to guide the learning process, which allows it to converge despite the high dimensionality of the problem.
This method is compared to the classical control techniques, achieving better temporal and energetic efficiency.

<div align="center">
<img src="https://gitlab.inria.fr/auctus-team/publications/shared-paper-code/iros2022-brake/-/raw/master/figures/result1.png" width="900px">

Refer to the paper for more details.
</div> 
